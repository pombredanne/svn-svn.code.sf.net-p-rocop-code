import time
import sys
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from forms import RegisterForm
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django import forms
from django.contrib.auth import authenticate
from django.contrib import auth
from settings import *
from tasks import *
from threadRocop import *

# Users can upload maximum of 500KB of file
MAX_UPLOAD_SIZE=512000

class UploadFileForm(forms.Form):
    file = forms.Field(widget=forms.FileInput, required=True)
    #choicefield = forms.ChoiceField(widget=forms.RadioSelect(), choices=[['p', 'Simple Plain Text'], ['c', 'Source Code']])

class adminForm(forms.Form):
    url = forms.CharField(max_length=200)
    no_of_pages = forms.IntegerField()

@csrf_protect
def register(request): 
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            username = request.POST['username']
            password = request.POST['password1']
            user = authenticate(username=username, password=password)
            auth.login(request, user)
            request.session['username'] = username
            return HttpResponseRedirect("/file_check/")
    else:
        form = RegisterForm()
    return render_to_response("register.html", RequestContext(request, {'form':form}))

@csrf_protect
def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password1']
        form = UserCreationForm(request.POST)
        user = authenticate(username=username, password=password)
        if user is not None:
            request.session['username'] = username
            if username == "admin":
                return HttpResponseRedirect("/moderator/")
            auth.login(request, user)
            return HttpResponseRedirect('/file_check/')
    else:
        form = UserCreationForm()
    return render_to_response("login.html", RequestContext(request, {'form':form}))

def logout(request):
    auth.logout(request)
    #del request.session['username']
    return HttpResponseRedirect("/")

@csrf_protect
def file_check(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/accounts/login/')

    if 'file' in request.FILES:
        try:
            file = request.FILES['file']
            # choice = request.POST['choicefield']
            
            # content_type of .txt file is text/plain
            if file.content_type not in ('application/octet-stream', 'text/plain'):
                raise forms.ValidationError('File Type Not Supported!!!') 
            username = request.session['username']
            filesize = file.size
            # print 'filesize=',filesize
            if filesize == 0:
                error = "Whoops!!! file is empty"
                return render_to_response('file_check.html', RequestContext(request, {'error':error, 'username':username}))
            elif filesize>MAX_UPLOAD_SIZE:
                error = "Please keep your file size below 500KB"
                return render_to_response('file_check.html', RequestContext(request, {'error':error, 'username':username}))
            else:
                filename = file.name
                fp = open('%s/%s' % (MEDIA_ROOT, filename), 'wb')
                for chunk in file.chunks():
                    fp.write(chunk)
                fp.close()

                # Once this file upload is over
                # call a thread handler here 
                startTime = time.time()
                #l = spawnThread(request.session['username'], filename)
                l = handle_backend_processing(10, username, filename)
                if l == 0:
                    error = 'Contents inside your file is too less to detect plagiarism.'
                    return render_to_response('file_check.html', RequestContext(request, {'username':username, 'error':error}))
                diffTime = time.time()-startTime
                print "TOTAL TIME=", diffTime
                request.session['filename'] = filename
                request.session['l'] = l
                return HttpResponseRedirect('/result/')
                #return render_to_response('result.html', {'match_doc_list':l, 'username':username, 'filename':filename})
        
        except forms.ValidationError:
            error = "File type is not supported. Please provide plain/text files."
            username = request.session['username']
            return render_to_response('file_check.html', RequestContext(request, {'error':error, 'username':username}))
    else:
        form = UploadFileForm()
        username = request.session['username']
    return render_to_response('file_check.html', RequestContext(request, {'form':form, 'username':username}))

def result(request):
    try:
        username = request.session['username']
    except KeyError:
        return HttpResponseRedirect('/accounts/login/')
    try:
        filename = request.session['filename']
    except KeyError:
        return HttpResponseRedirect('/file_check/')
    l = request.session['l']
    return render_to_response('result.html', {'username':username, 'allInfo':l, 'filename':filename})

def report(request):
    try:
        username = request.session['username']
    except KeyError:
        return HttpResponseRedirect('/accounts/login/')
    try:
        filename = request.session['filename']
    except KeyError:
        return HttpResponseRedirect('/file_check')
    try:
        l = request.session['l']
    except keyError:
        return HttpResponseRedirect('/file_check/')
    try:
        fileContent = l[2]
        return render_to_response('report.html', {'username':username, 'filename':filename, 'file_content':fileContent})
    except IndexError:
        #f = open("/home/kailash/workspace/rocop/temp/%s"%filename, "r")
        f = open("%s/%s"%(MEDIA_ROOT, filename), "r")
        fileContent = f.read()
        fileContent = "".join([c for c in fileContent if c not in('\n', '\r')])
        return render_to_response('report.html', {'username':username, 'filename':filename, 'file_content':fileContent})

def moderator(request):
    try:
        username = request.session['username']
    except KeyError:
        return HttpResponseRedirect('/accounts/login/')
    if username != 'admin':
        return HttpResponseRedirect("/file_check/")
    #repo_path = "/home/kailash/workspace/rocop/repo" 
    #f = open("%s/fp.log"%repo_path, "r")
    f = open("%s/fp.log"%REPO_PATH, "r")
    #gen_obj = os.walk("%s"%repo_path)
    gen_obj = os.walk("%s"%REPO_PATH)
    repo_dir_tree = gen_obj.next()
    dirs = repo_dir_tree[1] 
    msg = None;
    form = adminForm()
    try:
        update_flag = request.session['update_flag']
        update_msg = "Fingerprints insertion in progress"
    except KeyError:
        update_flag = 0
    if len(dirs) == 0:
        empty_msg = "Whoops!!! EMPTY"
    else:
        empty_msg = None
    if update_flag == 1:
        saveFP.apply_async()
        del request.session['update_flag']
        return render_to_response('admin.html',  RequestContext(request, {'form':form, 'username':username, 'sites':dirs, 'empty_msg':empty_msg, 'update_msg':update_msg}))
    if request.method == 'POST':
        url = request.POST['url']
        msg = "Crawler is running in background. Downloading pages from %s."%url
        numberOfPages = request.POST['no_of_pages']
        splittedAddress = url.split('//')
        if url == '' or numberOfPages == None or splittedAddress[0] == 'http:'or not numberOfPages.isdigit():
            invalid_msg = "Invalid data in the field."
            return render_to_response('admin.html',  RequestContext(request, {'form':form, 'username':username, 'sites':dirs, 'empty_msg':empty_msg, 'invalid_msg':invalid_msg}))
        #call the crawler from here
        crawl.apply_async(args=[str(url), int(numberOfPages)])
        #send_task("tasks.crawl", [str(url), int(numberOfPages)])
        #crawl(str(url), int(numberOfPages))
    return render_to_response('admin.html',  RequestContext(request, {'form':form, 'username':username, 'sites':dirs, 'empty_msg':empty_msg, 'msg':msg}))

def update_fp(request):
    try:
        username = request.session['username']
        if username == "admin":
            request.session['update_flag'] = 1
            return HttpResponseRedirect("/moderator/")
        else:
            return HttpResponseRedirect("/file_check/")
    except KeyError:
        return HttpResponseRedirect('/')