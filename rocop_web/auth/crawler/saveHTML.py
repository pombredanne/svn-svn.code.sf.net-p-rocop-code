#!/usr/bin/python
#
# crawler/saveHTML.py - saves the HTML pages of the URLs
#
# Copyright (c) 2010, ROCOP
#

#
# Imports
#

import sys
import chilkat
from textExtractor import * 

#
#
#

#
# The `saveDoc` method
#

def saveDoc(url, name, folder_path):
    '''
    Downloads the HTML page from the URL
    '''    
    http = chilkat.CkHttp()

    http.put_ProxyLogin("063bct512")
    http.put_ProxyDomain("10.100.0.1")
    http.put_ProxyPassword("k")
    http.put_ProxyPort(8080)

    success = http.UnlockComponent("Anything for 30-day trial")
    if (success != True):
        print http.lastErrorText()
        sys.exit()
    localPath = "%s/%d.html"%(folder_path,name)
    success = http.Download(url, localPath)
    convert_to_text(localPath, name, url, folder_path)

    if (success != True):
        print http.lastErrorText()
    else:
        print "Page Download Complete!"
