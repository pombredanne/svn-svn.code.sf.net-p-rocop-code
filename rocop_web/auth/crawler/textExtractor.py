#!/usr/bin/python
#
# crawler/textExtractor.py - extracts only the text from the HTML file omitting
#                            HTML tags and stores as a .txt file
#
# Copyright (c) 2010, ROCOP
#

#
# Imports
#

import sys
import chilkat
import os

#
#
#

#
# The `convert_to_text` method
#

def convert_to_text(path_to_read_html, name, url, folder_path):
    '''
    Reads the HTML file, extracts only the text from it and writes a new .txt file
    '''
    h2t = chilkat.CkHtmlToText()
    
    #  Any string argument automatically begins the 30-day trial.
    success = h2t.UnlockComponent("30-day trial")
    if (success != True):
        print h2t.lastErrorText()
        sys.exit()

    #  Set the HTML:
    try:
        fp = open(path_to_read_html, "r")
        fpStr = fp.read()
        newlineRemoved = "".join([c for c in fpStr if c not in('\n')])

        path_to_write_text = "%s/%d.txt"%(folder_path,name)
        plainText = h2t.toText(newlineRemoved)
        fp.close()

        fp = open(path_to_write_text, "w")
        fp.write(plainText)
        fp.close()

        f = open("%s/mapper.log"%folder_path, "a")
        toWrite = "%d"%name+";%s"%url+"\n"
        f.write(toWrite)
        f.close()
    except IOError:
        print "Couldn't read the file"
    
