#!/usr/bin/python 
#
# fingerprint/fingerprintDBMaintainer.py - saves the fingerprint of the crawled
#                                          pages in the database fingerprint log
#                                          keeps track of sites whose fp are already saved 
#
# Copyright (c) 2010, ROCOP
#

#
# Imports
#

import os 
import time
from settings import *
from DBHandler import *
from fingerprintGenerator import * 

#
#
#

#
# The `fingerprintDBMaintainer` class
#

class fingerprintDBMaintainer:
    '''
    It's an action class which is responsible for maintaining the DB of fingerprints
    '''
    def saveFP(self):
        '''
        Saves the fingerprints of the latest webpages 
        i.e., webpages whose fingerprints are not inserted into the database
        Maintains a fp.log file to keep track of websites the webpages of whose
        fingerprints are stored in the database
        '''
        #repo_path = "/home/kailash/workspace/rocop/repo" 
        #f = open("%s/fp.log"%repo_path, "r")
        f = open("%s/fp.log"%REPO_PATH, "r")
        fp_urls_stored = f.read()
        c = "".join([l for l in fp_urls_stored if l not in ("\n")])
        fp_urls_stored = c.split(';')
        f.close()

        gen_obj = os.walk("%s"%REPO_PATH)
        repo_dir_tree = gen_obj.next()
        dirs = repo_dir_tree[1] 
        totalTime = 0
        
        for site in dirs: 
            if site not in fp_urls_stored:
                # generate the fingerprint of all .txt files inside the website's directory
                path = "%s/%s"%(REPO_PATH, site)
                txtFilesList = os.walk(path).next()[2]
                for page in txtFilesList:
                    idList = page.split('.')
                    if page.endswith('.txt'):
                        ff = open("%s/%s"%(path,page), "r")
                        rawString = ff.read()
                        refinedString = ''.join([c for c in rawString if c not in ('\n', '\r', ' ')])
                        refinedString = refinedString.lower()
                        try:
                            newObj = FingerprintGenerator(refinedString)
                            newObj.generate_fingerprints()
                            dbHandler = DBHandler()
                            dbHandler.connect_to_db()
                            docId = "%s-%s"%(site,idList[0])
                            print docId
                            # Get the fingerprint and append it's position with document ID
                            tempTime = time.time()
                            for fprintList in newObj.fingerprints:
                                dbHandler.update_fingerprint_db(fprintList[0], docId)
                            dbHandler.insert_records_into_db()
                            diffTime = time.time()-tempTime
                            totalTime += diffTime
                        except IndexError:
                            print "Technical difficulty: Cannot generate fingerprints for %s"%site 
                f = open("%s/fp.log"%REPO_PATH, "a")
                f.write("%s;"%site)
                f.close()
        print 'totalTime=',totalTime

newMaintainer = fingerprintDBMaintainer()
newMaintainer.saveFP()

