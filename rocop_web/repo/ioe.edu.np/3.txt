

Home About IOE Alumni Virtual Tour Feedback Search
January 08, 2011


About IOE
Colleges
Autonomous Bodies
Resources
Research
Alumni
Conference/Trainings
Virtual Tour
News/Bulletins
Notice Board
Advanced Search
Feedback
Exam Schedules
Exam Results
CIT CARD CED IECS IRCC CDS
CPS CES CIMDU



Centre for Disaster Studies

The Center for Disaster Studies has been set up in the Institute of Engineering
only last year.(2003) Nepal looses significant number and amount of life as well
as property respectively to disaster every year. The major disasters are
landslide and flood. It has thus an objective of working for management of
disaster in the country. This Center is trying to do this through offering short
courses, carrying research and consultancy works in the beginning. The ultimate
aim is to begin a Masters Course in Disaster Management in the Institute.

