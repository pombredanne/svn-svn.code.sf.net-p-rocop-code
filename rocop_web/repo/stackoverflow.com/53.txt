
$(function() { notify.showFirstTime('Q&A for professional and enthusiast
programmers'); });
Stack Exchange
log in | careers | chat | meta | about | faq

Stack Overflow
    Questions
    Tags
    Users
    Badges
    Unanswered
    Ask Question

A 32-bit unsigned integer in NSDictionary

up vote 1 down vote favorite

Hello. I am developing LiveJournal.com client and i have one question. Can u,
please, explain what does it mean?

scalar Relevant when security is usemask. A 32-bit unsigned integer representing
which of the user's groups of friends are allowed to view this post. Turn bit 0
on to allow any defined friend to read it. Otherwise, turn bit 1-30 on for every
friend group that should be allowed to read it. Bit 31 is reserved.

What am i need to add in NSMutableDictionary if i want to see, for example,
group with id=6? I don't understand...

objective-c int unsigned
link | flag
asked Jan 1 at 9:35
Stas Dmitrenko
16 2

1 Answer

active newest votes
up vote 1 down vote

The security implementation works by authorizing or no a group of friends to
read a journal/text.

This assume there is a maximum of 30 possible groups, as bit-0 is specific, and
bit-31 is reserved.

The security is coded on an unsigned int , meaning 32 bits, of which bit0 to
bit31 have a special meaning.

    the LSb, bit0, if set to 1 allows any friend the read access.
    if bit0 is 0 , it means that the next 30 bits, bit1 to 30, are checked to
    see if the group i (from bit i ) has read access (bit set to 1 ) or no (bit
    set to 0 ).

An unsigned int can be represented as

bit 31                     bit 0  v                              v  bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb

for instance the decimal value of 11 (0...01011, or 8+3) is represented as

00000000000000000000000000001011

where bit 0, 1 and 3 are set to 1, the others are set to 0.

So, to check if a friend has access to a journal, take the journal access bits,
for instance

11000000000000000000000000001011

you see that the first bit is 1, so all groups (all friends) are authorized.

If the access would be

11000000000000000000000000001010

the first bit is 0, so you have to check the group number against the access
above. Say the group number is 1, you check against the access above

11000000000000000000000000001010                                1                                ^                        group one

and you see that the group 1 is authorized. If you would have

11000000000000000000000000001000

the group 1 is not authorized.

For groups 3 and 30, the access is also authorized:

11000000000000000000000000001000   *                          *

(The MSb. bit31 is set to 1 , but its value is reserved, so you shouldn't bother
its value)

link | flag
edited Jan 1 at 10:03

answered Jan 1 at 9:57
ring0
2,236 2 8

WOW. BIG BIG BIG thank you! Now everything is working! U are the best!))) �
Stas Dmitrenko Jan 1 at 10:12

Don&#39;t forget to accept the answer if you think it deserves it :-) � ring0
Jan 1 at 13:09

Your Answer

var enableImageUploads = true; $(function() { editorReady('answer', true/*
confirm navigation after wmd keypress */); });
draft saved
log in master.onClickDraftSave('#login-link');
or
Name
Email never shown
Home Page

Not the answer you're looking for? Browse other questions tagged objective-c int
unsigned or ask your own question .

Hello World!

Stack Overflow is a collaboratively edited question and answer site for
programmers � regardless of platform or language. It's 100% free, no
registration required.

about � faq �

tagged

objective-c � 25825
int � 307
unsigned � 120

asked

4 days ago

viewed

38 times

latest activity

4 days ago

var z = document.createElement("script"); z.type = "text/javascript"; z.async =
true; z.src =
"http://engine2.adzerk.net/z/8277/adzerk1_2_4_43,adzerk2_2_17_45,adzerk3_2_4_44?k
eywords=objective-c,int,unsigned"; var s =
document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(z, s);
(function(){var
g="http://careers.stackoverflow.com/Ad/js",c=location.search=="?golarge",a=locati
on.hash.indexOf("#ninjas")>-1,b="Pretty";(function(){var
b="min-height:248px;margin:15px
0",c=$("<div>").addClass("hireme").attr("style",b),a=$("<div>").html(c);document.
write(a.html())})();$(function(){a&&$("#adzerk1").html("").append($("<div>").attr
("id","herebeninjas")).attr("id","stop-adzerk");setTimeout(i,1200);setTimeout(h,3
00)});var h=function(){c=c||e();a=a||d();b=c?"Large":b;f()},i=function(){var
a=$("div.hireme");a.each(function(){$(this).html().replace("
","").length==0&&$(this).remove()})},e=function(){return
$("#careersadsdoublehigh").length>0},d=function(){return
$("#herebeninjas").length>0},f=function(){var
a=$("<script>").attr("src",j()).attr("type","text/javascript");$("body").append(a
)},j=function(){return g+"?style="+b+"&gobanner="+a.toString()}})();

Related

How to cast a 32-bit integer from unsigned to signed in MySQL or PHP?
32 bit unsigned int php
Using ruby to convert unsigned integers stored as signed back to the original
value
Signed versus UnSigned Integers
PHP&#39;s unsigned integer on 32 bit and 64 bit platform
Convert an unsigned 16 bit int to a signed 16 bit int in C#
64-bit unsigned to 32-bit signed
Practical example of 32 bit unsigned, signed, 64 bit differences
What is the maximum value of an unsigned 64-bit integer in English?
Get the signed/unsigned variant of an integer template parameter without
explicit traits
Signed und Unsigned Integers in Preon
Compute the absolute difference between unsigned integers using SSE
Unsigned Integer in Javascript
Arithmetic operations on unsigned and signed integers
JNA unsigned integer by reference gives strange results
MySQL integer unsigned arithmetic problems?
How to declare 8-bit unsigned integer in ruby?
How do you store unsigned 64-bit integers in SQL Server?
double precision integer subtraction with 32-bit registers(MIPS)
Pros and cons of ways of storing an unsigned int without an unsigned int data
type
How is unsigned int/long represented
Why doesn&#39;t Java support unsigned ints?
How to create a string representing a Java long as though it were unsigned
64-bit value
Why compiler is not giving error when signed value is assigned to unsigned
integer? - C++
Preferred way to fix &ldquo;comparison between signed and unsigned integer�
warnings?
question feed
lang-c
about | faq | new blog | data | podcast | legal | advertising info | contact us
| feedback always welcome
&#9632; stackoverflow.com &#9632; api/apps &#9632; careers &#9632;
serverfault.com &#9632; superuser.com &#9632; meta &#9632; area 51 &#9632;
webapps &#9632; gaming &#9632; ubuntu &#9632; webmasters &#9632; cooking &#9632;
game development &#9632; math &#9632; photography &#9632; stats &#9632; tex
&#9632; english &#9632; theoretical cs &#9632; programmers
revision: 2011.1.6.1
site design / logo � 2011 stack overflow internet services, inc; user
contributions licensed under cc-wiki with attribution required
Stack Overflow works best with JavaScript enabled
var
_gaq=_gaq||[];_gaq.push(['_setAccount','UA-5620270-1']);_gaq.push(['_trackPagevie
w']);(function(){var
ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.sr
c='http://www.google-analytics.com/ga.js';var
s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})()
;_qoptions={qacct:"p-c1rF4kxgLUzNc"};