
$(function() { notify.showFirstTime('Q&A for professional and enthusiast
programmers'); });
Stack Exchange
log in | careers | chat | meta | about | faq

Stack Overflow
    Questions
    Tags
    Users
    Badges
    Unanswered
    Ask Question

Top Questions

interesting 153 featured hot week month
0
votes
0
answers
3
views

C# : How to open Video Device property windows?

c# property window video-streaming
4s ago Jason Down 2,207
0
votes
0
answers
3
views

Looking for infinite scroll to pan page content up/down and progressively load
in both directions

javascript jquery website
17s ago John K 5,869
0
votes
0
answers
0
views

UIWebView loadHTMLString shows blank screen

iphone iphone-sdk-4.0 uiwebview
22s ago futureelite7 1,623
0
votes
1
answer
8
views

Abstracting functions / avoiding repetition in functions

scheme racket plt-scheme
32s ago Yasir Arsanukaev 1,232
0
votes
0
answers
0
views

Is there a JQuery plugin that turns radio buttons into nice vertical selection?

jquery css templates plugins javascripot
47s ago TIMEX 4,412
0
votes
0
answers
5
views

Need an email form to be sent to two different recipients depending on what
selection has been made

php forms
1m ago Brent Friar 1,104
0
votes
0
answers
3
views

Question regarding App.Config for class library and logging

c#
1m ago Jason Down 2,207
1
vote
1
answer
8
views

Jquery .next() function not working

jquery
1m ago Sundhar 13
0
votes
2
answers
10
views

IsBadReadPtr analogue on Unix

c debugging unix libc
2m ago caf 43.6k
1
vote
0
answers
2
views

how to programmatically add spaces in SMS in iPhone SDK

iphone cocoa-touch iphone-sdk-4.0 sms
2m ago PARTH 911
1
vote
0
answers
3
views

Is there any way to invoke a JSP2 tag file from Java?

java jsp-tags
3m ago Peter Hart 837
1
vote
0
answers
4
views

What is CSS, Themes and Skin in ASP.Net?

c# asp.net css themes skin
3m ago thevan 43
1
vote
0
answers
2
views

Regarding click on hyperlink and button using HtmlUnit Driver in java
application

exception
3m ago d5111 11
1
vote
0
answers
2
views

Why does Apache complain that CGI.pm has panicked at line 4001 due to a memory
wrap?

perl apache error
4m ago GeneQ 897
1
vote
0
answers
2
views

Writing Code in the Application Xcode

c# c xcode
4m ago Jack 6
1
vote
0
answers
3
views

Accelerator for JButton

java swing jbutton
5m ago Rendicahya 224
0
votes
1
answer
6
views

VSFTPD: Cannot figure this thing out&hellip;

linux ubuntu ftp
6m ago Raghuram 2,248
1
vote
0
answers
10
views

Deploying IIS Settings with Web Deploy

visual-studio-2010 iis deployment virtual-directory web-deploy
6m ago qntmfred 2,128
0
votes
0
answers
2
views

PHP SOAP Attributes .net Web Service

php .net soap types complex
7m ago claw 1
0
votes
0
answers
2
views

donloading gif giles in iphone - improving performance

iphone-sdk-4.0
7m ago akila 1
0
votes
0
answers
2
views

email csv file using rails

ruby-on-rails
8m ago user385948 36
0
votes
0
answers
2
views

FAIL - Application at context path /Project could not be started

java tomcat
8m ago user563564 1
0
votes
0
answers
4
views

Emulating device firmware on ubuntu

qemu device-emulation
9m ago user488244 1
0
votes
0
answers
4
views

SQLite3 : How to create db via code?

database cocoa sqlite3
10m ago Miraaj 142
1
vote
0
answers
2
views

how to validate web services getting from .net web server

iphone iphone-web
10m ago MaheshBabu 468
0
votes
1
answer
6
views

edit form routing - on update submit runs #show method instead of #update

ruby-on-rails ruby-on-rails3
11m ago Beerlington 2,461
0
votes
0
answers
4
views

Faster way to convert from a String to generic type T when T is a valuetype?

vb.net
11m ago Kumba 195
0
votes
1
answer
7
views

Is it really impossible to read Http Headers in ActionScript?

actionscript http-headers flash-player
11m ago Lars 1,845
1
vote
3
answers
24
views

Is it possible to build this type of program in PHP?

php
12m ago mlaw 441
0
votes
0
answers
1
view

How to create unit test for Paperclip::Geometry?

ruby-on-rails unit-testing paperclip shoulda
12m ago DigMe 1
1
vote
1
answer
13
views

VB .NET picture GetPixel & SetPixel: Include alpha?

vb.net alpha getpixel
14m ago Chris Haas 2,646
0
votes
1
answer
13
views

Android inter-Activity signalling (newbie question)

android
14m ago Leif Andersen 761
0
votes
2
answers
6
views

How see the converted sql from ActiveRecord method in view, etc

ruby-on-rails
14m ago glongman 146
0
votes
0
answers
17
views

Using UIAddressBook with TabBar and Navigation Controller

iphone cocoa-touch uinavigationcontroller
14m ago Ali 31
0
votes
0
answers
4
views

What is DependancyProperty for in terms of SharePoint Workflow ?

sharepoint workflow-foundation sharepoint-workflow
15m ago Nikhil Vaghela 203
0
votes
0
answers
6
views

How to Implement the given-Solution to solve circular Navigation problem

windows-phone-7
17m ago MilkBottle 21
0
votes
0
answers
8
views

How to access ethernet port using VC++? without using sockets.

visual-studio visual-c++ mfc vc++.net raw-ethernet
18m ago Zero Cool 21
1
vote
2
answers
32
views

Entity Framework in layered architecture

c# architecture entity-framework-4 multiplayer multi-tier
19m ago taher chhabrawala 108
3
votes
1
answer
33
views

Highcharts equivalent of this Raphael.js chart demo � possible?

javascript raphael highcharts
20m ago Wahnfrieden 1,340
0
votes
0
answers
10
views

How to write formula in Excel sheet which is stored in the perl variable.?

perl excel
20m ago Enrique 2,080
1
vote
1
answer
6
views

Is it necessary to var scope loop variables in CFScript?

coldfusion coldfusion-9
21m ago Adam Tuttle 4,436
0
votes
0
answers
4
views

Custom ASPNetMembership FailureInformation always null, OnValidatingPassword
issue

asp.net .net-4.0 asp.net-membership asp.net-4.0
21m ago Greg 6,151
2
votes
1
answer
15
views

How to make all controls of a column the same width

html css
23m ago Oliver A. 530
2
votes
2
answers
23
views

Is there a service provider for windows based web hosting that allows PHP COM
class?

php windows web com hosting
23m ago Robert Springfield 21
0
votes
0
answers
3
views

Ninject problem binding to constant value in MVC3 with a RavenDB session

mvc ninject ravendb
24m ago Jim 2,047
0
votes
0
answers
8
views

Is AfxBeginThread safe?

multithreading visual-c++ mfc
24m ago Casebash 2,775
0
votes
0
answers
2
views

Zend framework. how to pass options to custom front controller plugin?

zend-framework zend zend-application
25m ago Andre 206
0
votes
1
answer
7
views

how to add a select box in a region of an created page in drupal

drupal
25m ago Nikit 2,607
0
votes
0
answers
4
views

The field value is 1 or true in solr search results

lucene solr
26m ago KailZhang 26
0
votes
0
answers
2
views

Persistent store

blackberry persistence object-persistence
27m ago user469999 35
0
votes
0
answers
4
views

Working with datetime type in Quickbooks My Time files

php xml osx datetime quickbooks
27m ago jakemcgraw 4,400
0
votes
0
answers
12
views

VB .NET: PictureBox scales up my images!

vb.net zoom scale picturebox
27m ago Omega 49
3
votes
1
answer
30
views

Accessing vars from another clojure namespace?

clojure settings
28m ago erikcw 660
0
votes
1
answer
6
views

How popular is GRAILS BeanBuilder for replacing xml based spring configuration?

java spring grails j2ee java-ee
29m ago Burt Beckwith 6,710
0
votes
0
answers
7
views

implementing ASp.Net VOIP

asp.net-mvc
30m ago Daniel 1
2
votes
4
answers
77
views

Redirecting user to login page if not authenticated

c# .net forms-authentication
30m ago Greg 6,151
0
votes
0
answers
5
views

Stylesheet to add missing element ID&#39;s to a svg file

svg
30m ago Fisad 1
0
votes
0
answers
5
views

J2ME, Bluetooth for mobiles

java-me bluetooth
31m ago user564935 1
0
votes
6
answers
872
views

this class is not key value coding-compliant for the key

iphone error value key
33m ago D Carney 473
1
vote
0
answers
8
views

wpf dragenter/dragleave problem

wpf drag-and-drop
34m ago VHanded 52
0
votes
1
answer
10
views

Obtaining and storing a machine name in a variable

windows variables batch
35m ago Robert 1,647
0
votes
0
answers
3
views

Converting a Rails 2 project into a Rails 3 project

ruby-on-rails3
35m ago James Ziggs 1
0
votes
4
answers
22
views

What is the CSS syntax for a named span class nested in a div?

html css
36m ago Patrick Evans 178
0
votes
0
answers
3
views

Deleting old comments from Drupal 5 site

drupal comments drupal-5
38m ago HKBemis 1
1
vote
3
answers
26
views

C - how to set default values for types in C headers

c header types values
38m ago Michael Burr 73.1k
1
vote
0
answers
9
views

linq vb.net getting rows with repeated data (only 2 out of 5 columns)

vb.net linq
38m ago kaytea 6
0
votes
1
answer
14
views

Beginner Question on traversing a EF4 model

entity-framework-4 linq-to-entities self-tracking-entities
42m ago cpedros 657
3
votes
2
answers
25
views

<code> vs <pre> vs <samp> for inline and block code snippets.

html css
44m ago jleedev 13.9k
0
votes
0
answers
2
views

JSF 1.1 data table header freezing issue. Header is moving up when scroll down
in to the data table.

jsf
44m ago Pradip 1
1
vote
1
answer
4
views

How to import specific part from multi parts in MEF?

mef
45m ago Josh Einstein 20.6k
0
votes
0
answers
12
views

floating vs absolutely positioning conundrum

jquery css float absolute-positioning
45m ago JCHASE11 483
0
votes
0
answers
6
views

Timezone offset for $formatDate = $.PHPDate(&ldquo;U�, date);

javascript jquery date
47m ago Tim 171
0
votes
0
answers
3
views

How IMAP idle works?

ruby heroku imap eventmachine
48m ago Asaxena 68
0
votes
0
answers
6
views

Re compiling java jars to a different version

compiler jar
48m ago Anand 143
2
votes
1
answer
10
views

XPath with optional tbody element

html xpath
49m ago Phrogz 5,381
0
votes
0
answers
4
views

sl4a xmpppy client not sending message to appengine xmpp client

python google-app-engine xmpp xmpppy sl4a
50m ago Garrows 28
0
votes
0
answers
5
views

Http live streaming for ipad, iphone and safari

ipad ffmpeg
54m ago Riyas Anchal 6
0
votes
0
answers
1
view

Openning a native web browser on Air desktop app

iframe webbrowser adobe-air
55m ago sweetier 22
0
votes
0
answers
8
views

MYSQL and Facebook (FBML)

php facebook fbml
57m ago user564921 1
1
vote
1
answer
8
views

How to force Android View to shrink children

android android-layout relativelayout
59m ago Jake Basile 100
0
votes
0
answers
8
views

Android Radio Button doesn&#39;t get checked sometimes.

android multithreading radio-button radio-group
59m ago Ragunath Jawahar 417
0
votes
0
answers
2
views

SharePoint Workflow to create document off of custom content type just creates
blank document

sharepoint wss-3.0
1h ago jbv 1
2
votes
1
answer
24
views

Error opening serial port in c#

c# .net serial-port serial
1h ago Jerry Davis 25
0
votes
0
answers
3
views

PHP5 giving failed to open stream: HTTP request failed error when using fopen.

php fopen
1h ago mickey 20
0
votes
0
answers
1
view

Setting init_connect to a string with spaces in an Amazon RDS Parameter Group
using the command line interface complains with malformed errors

amazon-rds rds
1h ago Lail 1
0
votes
0
answers
4
views

Edit ScriptCase Code Directly?

php scripting
1h ago David Savage 16
0
votes
0
answers
2
views

htmltextbox property .selectable=false doesnt work on actionscript 3

flash actionscript-3
1h ago DomingoSL 468
3
votes
1
answer
24
views

What JS objects can be added through appendChild()?

javascript dom dynamic dhtml extend
1h ago Jani Hartikainen 7,152
1
vote
1
answer
7
views

Fitnesse - Standard fitSharp configuration problem with .NET 4

.net fitnesse
1h ago Mike Stockdale 574
1
vote
0
answers
13
views

IQueryable<> dynamic ordering/filtering with GetValue fails

c# entity-framework
1h ago MyNameIsJob 249

Looking for more? Browse the complete list of questions , or popular tags . Help
us answer unanswered questions .

Hello World!

Stack Overflow is a collaboratively edited question and answer site for
programmers � regardless of platform or language. It's 100% free, no
registration required.

about � faq �

var z = document.createElement("script"); z.type = "text/javascript"; z.async =
true; z.src = "http://engine2.adzerk.net/z/8277/adzerk2_2_17_45"; var s =
document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(z, s);
(function(){var
g="http://careers.stackoverflow.com/Ad/js",c=location.search=="?golarge",a=locati
on.hash.indexOf("#ninjas")>-1,b="Pretty";(function(){var
b="min-height:248px;margin:15px
0",c=$("<div>").addClass("hireme").attr("style",b),a=$("<div>").html(c);document.
write(a.html())})();$(function(){a&&$("#adzerk1").html("").append($("<div>").attr
("id","herebeninjas")).attr("id","stop-adzerk");setTimeout(i,1200);setTimeout(h,3
00)});var h=function(){c=c||e();a=a||d();b=c?"Large":b;f()},i=function(){var
a=$("div.hireme");a.each(function(){$(this).html().replace("
","").length==0&&$(this).remove()})},e=function(){return
$("#careersadsdoublehigh").length>0},d=function(){return
$("#herebeninjas").length>0},f=function(){var
a=$("<script>").attr("src",j()).attr("type","text/javascript");$("body").append(a
)},j=function(){return g+"?style="+b+"&gobanner="+a.toString()}})();

Recent Tags

javascript � 80
ruby-on-rails � 33
java � 109
video � 2
scala � 6
c# � 130
authentication � 5
smtp � 2
eclipse � 15
ide � 3
iphone � 55
objective-c � 29
uialertview � 2
visual-studio-2010 � 13
iis � 7
deployment � 6
cocoa � 11
mvc � 10
design-patterns � 5
mvvm � 4
linq � 6
dynamic � 4
where-clause � 3
php � 63
android � 42
vb.net � 18
ruby � 21
ruby-on-rails-3 � 5
facebook � 12
blackberry � 3
java-me � 2
login � 3
git � 13
python � 48
architecture � 6
entity-framework-4 � 5
coldfusion � 6
.net � 58
asp.net � 54
security � 9
asp.net-membership � 2
hashing � 2
html � 33
css � 30
windows � 21
web � 2
com � 3
hosting � 2
jquery � 66
clojure � 9
forms-authentication � 2
arrays � 11
clustering � 2
pattern � 2
learning � 3
encryption � 4
wpf � 25
drag-and-drop � 4
eval � 2
exec � 2

all tags �

Recent Badges

Yearling Joakim Berglund

Necromancer mipadi

Enthusiast Tahbaza

Enlightened bgporter

Civic Duty Kurru

Fanatic Jason Webb

Notable Question sardaukar

Notable Question user84786

Tenacious Jonathan Wood

Enthusiast EBAG

Notable Question Steve

Notable Question Abs

Famous Question dirtside

Notable Question Luca Martinetti

Necromancer Paul

magento Alan Storm

c# Jay

javascript Fabien Ménager

Enlightened houbysoft

Notable Question user282476

Notable Question Greg Dean

Enthusiast Nico

Enthusiast Jumbogram

Enthusiast Max

Enthusiast Vicfred

Enthusiast Paul S

Enthusiast Pauli Østerø

Famous Question MarvinS

Notable Question Phil Nash

Famous Question trenton

Necromancer taksofan

Notable Question e-satis

all badges �

recent questions feed
about | faq | new blog | data | podcast | legal | advertising info | contact us
| feedback always welcome
&#9632; stackoverflow.com &#9632; api/apps &#9632; careers &#9632;
serverfault.com &#9632; superuser.com &#9632; meta &#9632; area 51 &#9632;
webapps &#9632; gaming &#9632; ubuntu &#9632; webmasters &#9632; cooking &#9632;
game development &#9632; math &#9632; photography &#9632; stats &#9632; tex
&#9632; english &#9632; theoretical cs &#9632; programmers
revision: 2011.1.6.1
site design / logo � 2011 stack overflow internet services, inc; user
contributions licensed under cc-wiki with attribution required
Stack Overflow works best with JavaScript enabled
var
_gaq=_gaq||[];_gaq.push(['_setAccount','UA-5620270-1']);_gaq.push(['_trackPagevie
w']);(function(){var
ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.sr
c='http://www.google-analytics.com/ga.js';var
s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})()
;_qoptions={qacct:"p-c1rF4kxgLUzNc"};