
$(function() { notify.showFirstTime('Q&A for professional and enthusiast
programmers'); });
Stack Exchange
log in | careers | chat | meta | about | faq

Stack Overflow
    Questions
    Tags
    Users
    Badges
    Unanswered
    Ask Question

Top Questions

interesting 153 featured hot week month
94
votes
61
answers
63
kviews

Printing 1 to 1000 without loop or conditionals

c++ c interview-questions
9h ago Eon 1
81
votes
13
answers
4
kviews

What&#39;s the rationale for null terminated strings?

c++ c
1d ago skaffman 64.8k
56
votes
8
answers
2
kviews

How is null + true a string ?

c# .net string null datatypes
dec 18 at 6:49 Brooks Moses 3,149
32
votes
7
answers
2
kviews

What would be the math symbol for representing a fraction?

html
dec 21 at 15:32 freejelly 66
21
votes
17
answers
2
kviews

Why should I use foreach instead of for (int i=0; i<length; i++) in loops?

c# java .net loops foreach
dec 12 at 12:12 chacham15 175
54
votes
15
answers
1
kviews

Beyond Stack Sampling: C++ Profilers

c++ qt optimization profiling profiler
6h ago deft_code 6,704
68
votes
6
answers
2
kviews

The case for or against .NET (the beast)

c# .net install
dec 17 at 17:51 Mystere Man 6,827
46
votes
8
answers
1
kviews

Why does >= return false when == returns true for null values?

c# comparison operators nullable
dec 9 at 23:18 Sparr 3,921
38
votes
12
answers
1
kviews

What is md5() for?

php web-development security passwords
dec 16 at 20:26 Charlie Sheen 1,238
15
votes
21
answers
885
views

In either C or C++, should I check pointer parameters for NULL?

c++ c null
dec 26 at 9:57 Jaywalker 580
67
votes
3
answers
8
kviews

How to sort my paws?

python image-processing
dec 29 at 5:33 Joe Kington 5,993
21
votes
16
answers
592
views

Which functions in the C standard library commonly encourage bad practice?

c security c99 standard-library
13h ago R.. 14.9k
33
votes
12
answers
1
kviews

How do you unit test the real world?

c++ unit-testing testing mocking
dec 30 at 8:08 dietbuddha 1,229
34
votes
4
answers
5
kviews

Why must the last part of an Objective-C method name take an argument (when
there is more than one part)?

objective-c selectors language-design
dec 27 at 17:20 bbum 32.2k
18
votes
6
answers
1
kviews

Why do c++ programmers use != instead of <

c++
dec 29 at 18:51 Marcus Lindblom 4,962
35
votes
7
answers
8
kviews

What do programmers mean when they say, &ldquo;Code against an interface, not an
object.�?

c# .net tdd inversion-of-control
dec 26 at 16:54 DasIch 702
18
votes
12
answers
811
views

Should a developer always use version control [closed]

c# version-control tfs
dec 17 at 8:53 Stompp 41
18
votes
10
answers
1
kviews

Good use case for Akka

java scala asynchronous use-case akka
10h ago CodeSniper 1
19
votes
15
answers
886
views

Most expressive algorithm for the history of computing class?

algorithm language-agnostic programming-languages history teaching
dec 16 at 16:07 Doodle 297
25
votes
14
answers
1
kviews

Code to read and learn from [closed]

java c++ c lisp code-reading
dec 24 at 7:13 Vijay Mathew 9,259
24
votes
10
answers
741
views

monad theory and Haskell

haskell
dec 25 at 10:15 Martin 141
24
votes
4
answers
875
views

Is it legal to recurse into main() in C++?

c++ recursion standards main
1d ago Hans Passant 128k
33
votes
8
answers
508
views

Maximum number of characters using keystrokes A, Cttrl+A, Ctrl+C and Ctrl+V

c++ algorithm interview-questions
5h ago Andrew 1,206
16
votes
14
answers
383
views
+100

Common programming mistakes for Objective-C developers to avoid?

objective-c
14h ago nevan 6,899
14
votes
13
answers
771
views

Code Golf: Validate Sudoku Grid

algorithm code-golf sudoku
jan 2 at 14:17 Giuseppe Ottaviano 492
20
votes
6
answers
695
views

why -3==~2 in C#

c# .net condition bitwise-operators twos-complement
dec 18 at 0:13 jleedev 13.9k
27
votes
5
answers
567
views

Operator overloading

c++ operators operator-overloading c++-faq
dec 14 at 10:52 sbi 41.5k
18
votes
5
answers
822
views

What is the benefit of purely functional data structure?

data-structures haskell functional-programming ocaml purely-functional
dec 25 at 22:45 Jon Harrop 5,687
6
votes
12
answers
491
views

Learning Perl or Java for Object Oriented Programming (OOP) [closed]

java perl oop
dec 17 at 17:11 hvgotcodes 9,200
3
votes
8
answers
556
views

How to produce a StackOverflow Exception with a few lines of code?

c# .net stackoverflowexception
dec 17 at 19:44 hilal 2,389
6
votes
13
answers
375
views

Determining if Memory Pointer is Valid - C++

c++ memory pointers free
19h ago Max Lybbert 5,685
13
votes
7
answers
391
views
+100

Tips for refactoring a 20K lines library

php oop library open-source refactoring
7h ago jblue 1,187
27
votes
4
answers
760
views

Is there any way to put malicious code into Regex?

regex security
2d ago Bruce Ediger 21
11
votes
16
answers
7
kviews

Could not find adb.exe - after upgrade to Android SDK 2.3

android eclipse adt android-sdk-2.3
dec 31 at 4:27 Deb 3
10
votes
13
answers
451
views

Moving ahead in python [closed]

python books learning
jan 2 at 19:18 Vicky T 21
15
votes
7
answers
588
views

Definitions of sqrt, sin, cos, pow etc. in cmath, C/C++

c++ c math cmath definitions
dec 28 at 10:58 David Heffernan 3,009
26
votes
2
answers
689
views

My java code has an obvious error. Why does it compile and run?

java syntax
dec 10 at 6:32 Meteor 11
9
votes
3
answers
2
kviews

How to split and join array in C++ for UDP?

c++ udp
dec 31 at 13:33 Steve-o 1,769
12
votes
10
answers
343
views

Memory Allocation Problem

c memory-allocation interview-questions
dec 10 at 7:15 ravi 738
11
votes
8
answers
507
views

Given Prime Number N, Compute the Next Prime?

algorithm math primes
dec 18 at 3:09 belisarius 10.2k
3
votes
8
answers
328
views

How to check if a char is equal to an empty space?

java char
dec 22 at 15:06 Stephen C 54k
13
votes
9
answers
478
views

I&#39;m maintaining a java class that&#39;s 40K lines long.. problem?

java maintenance legacy-code
jan 2 at 12:17 Tom Anderson 2,523
5
votes
11
answers
475
views

Why is C++ backward compatible with C ? Why isn&#39;t there some &ldquo;pure�
C++ language ?

c++ c history
jan 2 at 1:21 caf 43.6k
8
votes
8
answers
792
views

Why no i++ in Scala

scala
dec 23 at 17:43 Apocalisp 9,957
21
votes
10
answers
412
views

How do you keep debug code out of production?

php javascript debugging vbscript
dec 9 at 18:08 Anush Prem 267
21
votes
5
answers
395
views

I&#39;ve decided not to cater for IE6 - What tasty CSS treats can I use?

html css
dec 19 at 9:45 seengee 5,053
6
votes
9
answers
391
views

What does &ldquo;where� mean in a C# class declaration?

c# .net generics
2d ago Tomas Jansson 996
18
votes
8
answers
634
views

What is holding genetic programming back?

algorithm genetic-programming evolutionary-algorithm
dec 8 at 19:47 Josephine 526
13
votes
3
answers
7
kviews

How can I make VIM play typewriter sound when I write a letter?

linux vim vim-plugin
dec 12 at 20:46 BIAJ 13
5
votes
13
answers
842
views

What is preventing Scala from gaining mass popularity? [closed]

scala popularity
dec 19 at 0:49 Cybernd 31
12
votes
5
answers
367
views

Can a C program handle C++ exceptions?

c++ c exception-handling
dec 15 at 10:56 FrankH. 710
12
votes
8
answers
663
views

Puzzle on pointers [closed]

c++ c arrays pointers puzzle
dec 12 at 1:36 user434507 2,037
25
votes
5
answers
559
views

What is it about Fibonacci numbers?

algorithm data-structures fibonacci
jan 1 at 4:32 savalia 11
8
votes
12
answers
323
views

What are the alternatives to public fields?

java c++ class fields public
2d ago James 55
12
votes
11
answers
385
views

Neat way to write loop that has special logic for the first item in a
collection.

c# collections
dec 9 at 16:43 Ian Ringrose 7,756
23
votes
3
answers
380
views

Why use div or ldiv in C/C++?

c++ c
dec 30 at 19:15 nmichaels 7,458
20
votes
4
answers
2
kviews

Getting-started: Setup Database for Node.js

javascript database mongodb couchdb node.js
dec 30 at 2:22 Shripad K 1,448
13
votes
7
answers
375
views

Pong: How does the paddle know where the ball will hit?

algorithm artificial-intelligence pong
jan 2 at 15:34 Matt 11.5k
16
votes
6
answers
480
views

what is typeof((c) + 1) in C

c gcc
dec 14 at 19:00 Jens Gustedt 5,931
23
votes
4
answers
538
views

Mathematica: what is symbolic programming?

programming-languages haskell functional-programming mathematica
dec 14 at 9:37 Michael Kohl 2,165
7
votes
7
answers
497
views

Pure Functional Language: Haskell

haskell functional-programming
dec 27 at 17:47 Anon' 1
3
votes
4
answers
589
views

Can parser combinators be made efficient?

haskell f# parser-generator parser-combinators parsec
dec 31 at 0:23 Stephan Tolksdorf 81
14
votes
4
answers
333
views

Modify a given number to find the required sum?

c algorithm math sum puzzle
jan 2 at 10:27 Jeff M 5,745
26
votes
2
answers
375
views

Why trying to box references when comparing ?

c# generics comparison equality boxing
dec 18 at 15:28 Eric Lippert 75.4k
9
votes
7
answers
465
views

Check if one integer is an integer power of another

algorithm interview-questions arithmetic
dec 15 at 17:22 j_random_hacker 13.9k
11
votes
10
answers
432
views

How does java do modulus calculations with negative numbers?

java math numbers negative modulus
dec 10 at 20:34 starblue 14.7k
0
votes
5
answers
422
views

Super short and awesome variable name in C# [closed]

c# variables
dec 8 at 13:21 Will &#9830; 34.6k
10
votes
8
answers
521
views

The D Programming Language for Game Development

game-development d
jan 2 at 1:47 ponce 213
11
votes
5
answers
547
views

Why Scala good for concurrency?

scala concurrency language
dec 18 at 15:50 timday 6,722
9
votes
9
answers
526
views

As Java is to Scala, C++ is to &hellip; ?

java c++ c scala numerical-methods
dec 31 at 13:09 Anonymous 1
8
votes
11
answers
243
views

C++ method that can/cannot return a struct

c++ struct
dec 13 at 21:28 John Dibling 16.3k
10
votes
6
answers
303
views

Scala: short form of pattern matching that returns Boolean

scala pattern-matching
dec 14 at 17:40 extempore 2,604
16
votes
10
answers
339
views

CodeGolf: Find the Unique Paths

language-agnostic graph code-golf rosetta-stone
2d ago MtnViewMark 2,292
10
votes
6
answers
263
views

Number of parameters for a constructor

c++ design-patterns parameters refactoring constructor
1d ago Péter Török 33k
4
votes
12
answers
390
views

Better way to structure/new keyword

java c++
dec 17 at 10:51 aronp 131
6
votes
8
answers
383
views

ForEach() : Why can&#39;t use break/continue inside

c# foreach break continue
dec 14 at 13:42 Shadow Wizard 3,265
8
votes
7
answers
298
views

How to lowercase a string except for first character with C#

c#
dec 14 at 6:16 Vyas 387
3
votes
13
answers
328
views

Infinite Loops and Early Return Statements

c++ infinite-loop programming-practices
dec 20 at 18:01 supercat 3,106
14
votes
11
answers
414
views

Randomly permute N first elements of a singly linked list

c++ c algorithm math permutation
dec 21 at 20:15 Emilio M Bumachar 441
1
vote
8
answers
396
views

Are infinite lists useful for any real world applications?

list haskell lazy-evaluation infinite
dec 21 at 20:46 Conal 1,132
18
votes
4
answers
261
views

Is it possible to mark an assembly as deprecated?

c# .net
dec 17 at 23:08 dboarman 1,670
9
votes
11
answers
429
views

optimize mysql count query

sql mysql query-optimization
dec 27 at 15:45 andersonbd1 312
5
votes
7
answers
319
views

Download 3000+ Images Using C#?

c# asp.net image url download
2d ago Richard 22.3k
12
votes
6
answers
334
views

Why does System.IO.Path.Combine have 4 overloads?

c# .net
dec 19 at 21:42 k3b 576
12
votes
9
answers
410
views

What is a good Java based Content Management System for us, and why?

java cms jsr170 jsr283
dec 19 at 15:42 bakopanos costas 158
5
votes
6
answers
296
views

reason for memory leakage in C C++

c++ c memory-allocation
dec 15 at 8:54 Matthieu M. 16.9k
9
votes
6
answers
264
views

What is the console.log used for in jQuery?

jquery console firebug
dec 27 at 16:09 Mark Byers 108k
5
votes
7
answers
156
views

Using abs () method in java. My compiler doesn&#39;t know the method..

java
dec 10 at 23:18 Goran Jovic 1,677
11
votes
4
answers
370
views

In Java, why have a code block with no keywords, just curly brackets

java design-patterns
dec 14 at 19:26 Mike 1,904
7
votes
7
answers
186
views

java for loop not working

java syntax for-loop iteration
jan 1 at 16:46 Adil Mehmood 114
5
votes
9
answers
237
views

When have you used C++ &#39;mutable&#39; keyword?

c++
1d ago jeong 347
4
votes
7
answers
233
views

Pointer to local variable

c++ c pointers local-variables
jan 2 at 23:15 tp1 1
12
votes
7
answers
224
views

When to use a View instead of a Table?

sql database-design table view
dec 7 at 15:24 iDevlop 1,944
4
votes
9
answers
173
views

In order to know jQuery, do I have to know JavaScript first?

javascript jquery
10h ago Andrew Lewis 1,856
18
votes
0
answers
428
views

I still can&#39;t figure out how to program! [closed]

java python c
dec 29 at 22:26 onemasse 716
9
votes
12
answers
4
kviews

Android SDK installation doesn&#39;t find JDK

android sdk install x64 jdk
1d ago Martin English 1

Looking for more? Browse the complete list of questions , or popular tags . Help
us answer unanswered questions .

Hello World!

Stack Overflow is a collaboratively edited question and answer site for
programmers � regardless of platform or language. It's 100% free, no
registration required.

about � faq �

var z = document.createElement("script"); z.type = "text/javascript"; z.async =
true; z.src = "http://engine2.adzerk.net/z/8277/adzerk2_2_17_45"; var s =
document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(z, s);
(function(){var
g="http://careers.stackoverflow.com/Ad/js",c=location.search=="?golarge",a=locati
on.hash.indexOf("#ninjas")>-1,b="Pretty";(function(){var
b="min-height:248px;margin:15px
0",c=$("<div>").addClass("hireme").attr("style",b),a=$("<div>").html(c);document.
write(a.html())})();$(function(){a&&$("#adzerk1").html("").append($("<div>").attr
("id","herebeninjas")).attr("id","stop-adzerk");setTimeout(i,1200);setTimeout(h,3
00)});var h=function(){c=c||e();a=a||d();b=c?"Large":b;f()},i=function(){var
a=$("div.hireme");a.each(function(){$(this).html().replace("
","").length==0&&$(this).remove()})},e=function(){return
$("#careersadsdoublehigh").length>0},d=function(){return
$("#herebeninjas").length>0},f=function(){var
a=$("<script>").attr("src",j()).attr("type","text/javascript");$("body").append(a
)},j=function(){return g+"?style="+b+"&gobanner="+a.toString()}})();

Recent Tags

javascript � 80
ruby-on-rails � 33
iphone � 56
cocoa-touch � 2
iphone-sdk-4.0 � 4
sms � 2
java � 110
video � 2
scala � 6
c# � 130
authentication � 5
smtp � 2
eclipse � 15
ide � 3
objective-c � 29
uialertview � 2
visual-studio-2010 � 13
iis � 7
deployment � 6
cocoa � 11
mvc � 10
design-patterns � 5
mvvm � 4
linq � 6
dynamic � 4
where-clause � 3
php � 63
android � 42
vb.net � 18
ruby � 21
ruby-on-rails-3 � 5
facebook � 12
blackberry � 3
java-me � 2
login � 3
git � 13
python � 48
architecture � 6
entity-framework-4 � 5
coldfusion � 6
.net � 58
asp.net � 54
security � 9
asp.net-membership � 2
hashing � 2
html � 33
css � 30
windows � 21
web � 2
com � 3
hosting � 2
jquery � 66
clojure � 9
forms-authentication � 2
arrays � 11
clustering � 2
pattern � 2
learning � 3
encryption � 4
wpf � 25

all tags �

Recent Badges

Yearling Joakim Berglund

Necromancer mipadi

Enthusiast Tahbaza

Enlightened bgporter

Civic Duty Kurru

Fanatic Jason Webb

Notable Question sardaukar

Notable Question user84786

Tenacious Jonathan Wood

Enthusiast EBAG

Notable Question Steve

Notable Question Abs

Famous Question dirtside

Notable Question Luca Martinetti

Necromancer Paul

magento Alan Storm

c# Jay

javascript Fabien Ménager

Enlightened houbysoft

Notable Question user282476

Notable Question Greg Dean

Enthusiast Nico

Enthusiast Jumbogram

Enthusiast Max

Enthusiast Vicfred

Enthusiast Paul S

Enthusiast Pauli Østerø

Famous Question MarvinS

Notable Question Phil Nash

Famous Question trenton

Necromancer taksofan

Notable Question e-satis

all badges �

monthly questions feed
about | faq | new blog | data | podcast | legal | advertising info | contact us
| feedback always welcome
&#9632; stackoverflow.com &#9632; api/apps &#9632; careers &#9632;
serverfault.com &#9632; superuser.com &#9632; meta &#9632; area 51 &#9632;
webapps &#9632; gaming &#9632; ubuntu &#9632; webmasters &#9632; cooking &#9632;
game development &#9632; math &#9632; photography &#9632; stats &#9632; tex
&#9632; english &#9632; theoretical cs &#9632; programmers
revision: 2011.1.6.1
site design / logo � 2011 stack overflow internet services, inc; user
contributions licensed under cc-wiki with attribution required
Stack Overflow works best with JavaScript enabled
var
_gaq=_gaq||[];_gaq.push(['_setAccount','UA-5620270-1']);_gaq.push(['_trackPagevie
w']);(function(){var
ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.sr
c='http://www.google-analytics.com/ga.js';var
s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})()
;_qoptions={qacct:"p-c1rF4kxgLUzNc"};