
//** Smooth Navigational Menu- By Dynamic Drive DHTML code library:
http://www.dynamicdrive.com //** Script Download/ instructions page:
http://www.dynamicdrive.com/dynamicindex1/ddlevelsmenu/ //** Menu created: Nov
12, 2008 //** Dec 12th, 08" (v1.01): Fixed Shadow issue when multiple LIs within
the same UL (level) contain sub menus:
http://www.dynamicdrive.com/forums/showthread.php?t=39177&highlight=smooth var
ddsmoothmenu={ //Specify full URL to down and right arrow images (23 is
padding-right added to top level LIs with drop downs): arrowimages:
{down:['downarrowclass', '../includes/menu/down.gif', 23],
right:['rightarrowclass', '../includes/menu/right.gif']}, transition:
{overtime:300, outtime:300}, //duration of slide in/ out animation, in
milliseconds shadow: {enabled:false, offsetx:5, offsety:5}, ///////Stop
configuring beyond here/////////////////////////// detectwebkit:
navigator.userAgent.toLowerCase().indexOf("applewebkit")!=-1, //detect WebKit
browsers (Safari, Chrome etc) getajaxmenu:function($, setting){ //function to
fetch external page containing the panel DIVs var
$menucontainer=$('#'+setting.contentsource[0]) //reference empty div on page
that will hold menu $menucontainer.html("Loading Menu...") $.ajax({ url:
setting.contentsource[1], //path to external menu file async: true,
error:function(ajaxrequest){ $menucontainer.html('Error fetching content. Server
Response: '+ajaxrequest.responseText) }, success:function(content){
$menucontainer.html(content) ddsmoothmenu.buildmenu($, setting) } }) },
buildshadow:function($, $subul){ }, buildmenu:function($, setting){ var
smoothmenu=ddsmoothmenu var $mainmenu=$("#"+setting.mainmenuid+">ul")
//reference main menu UL var $headers=$mainmenu.find("ul").parent()
$headers.each(function(i){ var $curobj=$(this).css({zIndex: 100-i}) //reference
current LI header var $subul=$(this).find('ul:eq(0)').css({display:'block'})
this._dimensions={w:this.offsetWidth, h:this.offsetHeight,
subulw:$subul.outerWidth(), subulh:$subul.outerHeight()}
this.istopheader=$curobj.parents("ul").length==1? true : false //is top level
header? $subul.css({top:this.istopheader? this._dimensions.h+"px" : 0})
$curobj.children("a:eq(0)").css(this.istopheader? {paddingRight:
smoothmenu.arrowimages.down[2]} : {}).append( //add arrow images '<img src="'+
(this.istopheader? smoothmenu.arrowimages.down[1] :
smoothmenu.arrowimages.right[1]) +'" class="' + (this.istopheader?
smoothmenu.arrowimages.down[0] : smoothmenu.arrowimages.right[0]) + '"
style="border:0;" />' ) if (smoothmenu.shadow.enabled){
this._shadowoffset={x:(this.istopheader?$subul.offset().left+smoothmenu.shadow.of
fsetx : this._dimensions.w), y:(this.istopheader?
$subul.offset().top+smoothmenu.shadow.offsety : $curobj.position().top)} //store
this shadow's offsets if (this.istopheader) $parentshadow=$(document.body) else{
var $parentLi=$curobj.parents("li:eq(0)") $parentshadow=$parentLi.get(0).$shadow
} this.$shadow=$('<div class="ddshadow'+(this.istopheader? ' toplevelshadow' :
'')+'"></div>').prependTo($parentshadow).css({left:this._shadowoffset.x+'px',
top:this._shadowoffset.y+'px'}) //insert shadow DIV and set it to parent node
for the next shadow div } $curobj.hover( function(e){ var
$targetul=$(this).children("ul:eq(0)")
this._offsets={left:$(this).offset().left, top:$(this).offset().top} var
menuleft=this.istopheader? 0 : this._dimensions.w
menuleft=(this._offsets.left+menuleft+this._dimensions.subulw>$(window).width())?
 (this.istopheader? -this._dimensions.subulw+this._dimensions.w :
-this._dimensions.w) : menuleft //calculate this sub menu's offsets from its
parent if ($targetul.queue().length<=1){ //if 1 or less queued animations
$targetul.css({left:menuleft+"px",
width:this._dimensions.subulw+'px'}).animate({height:'show',opacity:'show'},
ddsmoothmenu.transition.overtime) if (smoothmenu.shadow.enabled){ var
shadowleft=this.istopheader? $targetul.offset().left+ddsmoothmenu.shadow.offsetx
: menuleft var
shadowtop=this.istopheader?$targetul.offset().top+smoothmenu.shadow.offsety :
this._shadowoffset.y if (!this.istopheader && ddsmoothmenu.detectwebkit){ //in
WebKit browsers, restore shadow's opacity to full this.$shadow.css({opacity:1})
} this.$shadow.css({overflow:'', width:this._dimensions.subulw+'px',
left:shadowleft+'px',
top:shadowtop+'px'}).animate({height:this._dimensions.subulh+'px'},
ddsmoothmenu.transition.overtime) } } }, function(e){ var
$targetul=$(this).children("ul:eq(0)") $targetul.animate({height:'hide',
opacity:'hide'}, ddsmoothmenu.transition.outtime) if
(smoothmenu.shadow.enabled){ if (ddsmoothmenu.detectwebkit){ //in WebKit
browsers, set first child shadow's opacity to 0, as "overflow:hidden" doesn't
work in them this.$shadow.children('div:eq(0)').css({opacity:0}) }
this.$shadow.css({overflow:'hidden'}).animate({height:0},
ddsmoothmenu.transition.outtime) } } ) //end hover }) //end $headers.each()
$mainmenu.find("ul").css({display:'none', visibility:'visible'}) },
init:function(setting){ if (typeof setting.customtheme=="object" &&
setting.customtheme.length==2){ var mainmenuid='#'+setting.mainmenuid
document.write('<style type="text/css">\n' +mainmenuid+', '+mainmenuid+' ul li a
{background:'+setting.customtheme[0]+';}\n' +mainmenuid+' ul li a:hover
{background:'+setting.customtheme[1]+';}\n' +'</style>') }
jQuery(document).ready(function($){ //override default menu colors
(default/hover) with custom set? if (typeof setting.contentsource=="object"){
//if external ajax menu ddsmoothmenu.getajaxmenu($, setting) } else{ //else if
markup menu ddsmoothmenu.buildmenu($, setting) } }) } } //end ddsmoothmenu
variable //Initialize Menu instance(s): ddsmoothmenu.init({ mainmenuid:
"smoothmenu1", //menu DIV id //customtheme: ["#1c5a80", "#18374a"], //override
default menu CSS background values? Uncomment: ["normal_background",
"hover_background"] contentsource: "markup" //"markup" or ["container_id",
"path_to_menu_file"] })
    Home
    Company
        Our Vision, Mission & Goal
        Organization Chart
        Board of Directors
        Nepal Telecom in Brief
        Milestones
        Our Achievements
        Financial Statement
        Directorate
            Regional Directorates
                Kathmandu Regional Directorate
                Eastern Regional Directorate
                Central Regional Directorate
                Western Regional Directorate
                Mid-Western Regional Directorate
                Far-Western Regional Directorate
            IT Directorate
            Mobile Service Directorate
            PSTN Service Directorate
            Satellite Services Directorate
            Telecom Training Center
            Wireless Telephone Directorate
        Department
            Business Department
            Change Management
            Company Secretariat
            Finance
            Human Resource Mgmt
            Material Management Dept
            Operation & Maintenance
            Planning Department
            Rural Plan & Implement
            Transmission Department
        Publication
            Press Releases
            Newsletters
            MIS Report
            Telecom Smarika
            Annual Report
        Contact
    Basic Telephone
        PSTN Telephone
            Telephone
            ISDN Telephone
            Voice Mail Service
            Pay Phone
            Notice Board Service
            Tollfree - Phone
            Other VAS Services
        CDMA C-Phone
            Voice
            PDSN Data
            Short Message Service
    Mobile
        GSM Mobile Service
            GSM Post-Paid Mobile
            GSM Pre-Paid Mobile
            GPRS Service
            3G Service
            FnF Service
            International Roaming
            Caller Ringback Tones
        CDMA Mobile Service
            Post-paid Mobile
            Pre-paid SkyPhone
            Sky Data - Wireless Internet
            Broadband Wireless - EVDO
            CDMA Voice Mail Service
    Internet
        Dialup Internet
        ISDN Broadband Dialup
        Lease Connectivity
            Simple Leased Line
            Internet Leased Line
            Intranet Lease Line
        ADSL Broadband Service
            Volume Based
            Unlimited Monthly Plan
        GSM Mobile - Internet
            GSM Mobile - GPRS
            GSM Mobile - 3G
        CDMA Wireless - Internet
            Broadband Wireless - EVDO
            Sky Data - Wireless Internet
        Email
        WebSMS
        Services
        Change Password
    Other Services
        Rural Telephony
            KU Band
        Intelligent Network
            PCC Easy Call Service
            PSTN Credit Limit (PCL)
            Advanced Free-Phone Service
            Home Country Direct (HCD)
        IVR Services
        Notice Board Service
    Tariff
        PSTN Charges
        Trunk Charges SAARC Countries)
        PSTN Leased Charges
        Modem for PSTN Leased Line
        Inter Operator Call Charge
        International Call Tariff
        Internet Services
            ADSL Broadband Internet
            Dedicated Lease Connectivity
            Dialup Internet
        GSM Mobile Services
            GSM Mobile Call Tariff
            Inbound Roaming Charges
            Outbound Roaming Charges
            3 G Mobile Service
        CDMA Phone Services
            CDMA C-Phone Postpaid
            CDMA C-Phone Prepaid
            SKY Phone
    Notices
        Tender Announcement
        Telecom Notices
    Career
        Job Vacancy

Individuals | Corporates

Mile Stones of Nepal Telecom

--------------------------------------------------------------------------------


Year - - - - - Milestone Details
    1913 - - - - - Establishment of first telephone lines in Kathmandu
    1914 - - - - - Establishment of Open wire Trunk Link from Kathmandu to
    Raxaul (India)
    1935 - - - - - Installation of 25 lines automatic exchange in Royal Palace
    1936 - - - - - Installation of Open Wire Trunk line from Kathmandu to
    Dhankuta
    1950 - - - - - Establishment of Telegram Service
    1950 - - - - - Introduction to High frequency Radio System (AM)
    1950 - - - - - Establishment of CB telephone exchange (100 lines) in
    Kathmandu
    1951 - - - - - Installation of Open Wire Trunk line from Kathmandu to Palpa
    1955 - - - - - Distribution of telephone line to general public
    1962 - - - - - First Public Telephone Exchange in Kathmandu (300 lines CB)
    1964 - - - - - Beginning of International Telecommunications Service using
    HF Radio to India and Pakistan
    1965 - - - - - First Automatic exchange in Nepal (1000 lines in Kathmandu)
    1971 - - - - - Introduction of Telex Services
    1974 - - - - - Microwave transmission links establishment for internal trunk
    1982 - - - - - Establishment of Standard &quot;B&quot; Type Earth Station
    for international circuits
    1982 - - - - - Establishment of SPC telex exchange
    1983 - - - - - Establishment of digital Telephone Exchange
    1984 - - - - - Commencement of STD service
    1984 - - - - - Reliable Rural Telecom Service (JICA)
    1987 - - - - - Commencement of STD service
    1995 - - - - - Installation of Optical Fiber Network
    1996 - - - - - Conversion of all Transmission link to Digital transmission
    link
    1996 - - - - - Automation of the entire Telephone Network
    1996 - - - - - Independent Int. Gateway Exchange established
    1996 - - - - - Introduction of VSAT services
    1997 - - - - - Digital Link with D.O.T. India through Optical Fiber in
    Birgunj - Raxual
    1998 - - - - - Direct Link with Bangladesh
    1999 - - - - - Launching of GSM Mobile service
    2000 - - - - - Implementation of SDH Microwave Radio
    2000 - - - - - Launching of Internet Service
    2001 - - - - - Launching of Payphone Service
    2002 - - - - - East West Highway Optical Fiber Project
    2003 - - - - - GSM Prepaid Service
    2004 - - - - - NEPAL TELECOM (Transformation from Corporation to Nepal
    Doorsanchar Company Limited)
    2004 - - - - - Pre-paid Calling Card Service (IN Services)
    2005 - - - - - Soft launch of CDMA
    2005 - - - - - Access Network Services
    2005 - - - - - Outsourcing of Enquiry Service (197)
    2006 - - - - - PSTN Credit Limit Service - PCL (IN)
    2006 - - - - - Home Country Direct Service - NepalDirect (IN)
    2006 - - - - - MCC (198) Complaint Registration via IVR in Kathmandu Valley
    2006 - - - - - CDMA Limited Servies in Kathmandu Valley
    2007 - - - - - National Roaming for CDMA Mobile (Sky Phone) started
    2007 - - - - - Expansion of Internet Bandwidth via Optical link between
    Nepal & India
    2007 - - - - - PSTN Bill Enquiry Service started via 1606 IVR Service
    2007 - - - - - VOIP Call Complaint Registration started via 188 IVR Service
    2007 - - - - - GPRS, 3G and CRBT Services introduced in GSM Mobile
    2008 - - - - - Broadband ADSL Service launched
    2008 - - - - - IVR Service 1607 started for GSM and CDMA PUK Enquiry
    2008 - - - - - IVR 198 service extended for ADSL Fault Complaint
    Registration
    2008 - - - - - PSTN VMS � Notice Board Service Launched
    2009 - - - - - IVR 198 Service extended outside KTM valley
    2009 - - - - - IVR 1606 Service extended outside Kathmandu Valley
    2009 - - - - - SMS Service from GSM to CDMA mobile started
    2009 - - - - - Postpaid CDMA Mobile Service started
    2010 - - - - - EVDO Service started
    2010 - - - - - Soft Launch of EasyPhone IP Call Service

Online Accesses

    Check Mail
   
    Caller Ringback Tone
   
    GSM Mobile Bill
   
    Phone Bill
   
    PSTN VMS
   
    Telephone Search
   
    WebSMS
   
    Downloads
   

Quick Links

    Change Password
   
    PSTN Services
   
    Intelligent Network
   
    IVR Services
   
    CDMA Services
   
    Mobile Services
   
    Call Tariffs
   
    Active Tenders
   

� Copyright 2010, Nepal Doorsanchar Company Limited (Nepal Telecom)
Designed & Developed by Nepal Telecom, IT Directorate, Internet Section,
Jawalakhel, Lalitpur, Nepal

Best viewed with Mozilla Firefox Home | Sitemap | Downloads & Utilities |
Contact Info

